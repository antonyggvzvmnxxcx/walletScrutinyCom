---
name: Leo Wandersleb
short: lw
since: 2019-11-15
avatar: leo.jpg
---

Leo is the founder and main contributor to WalletScrutiny. Since 2015 he works
for Mycelium Android and is currently their release manager.
