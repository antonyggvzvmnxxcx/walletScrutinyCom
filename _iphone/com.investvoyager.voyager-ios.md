---
wsId: voyager
title: "Voyager - Buy Bitcoin & Crypto"
altTitle: 
authors:
- leo
appId: com.investvoyager.voyager-ios
appCountry: 
idd: 1396178579
released: 2019-02-13
updated: 2021-05-08
version: "2.9.18"
score: 4.76552
reviews: 59890
size: 59721728
developerWebsite: https://www.investvoyager.com/
repository: 
issue: 
icon: com.investvoyager.voyager-ios.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-02
reviewStale: true
signer: 
reviewArchive:


providerTwitter: investvoyager
providerLinkedIn: investvoyager
providerFacebook: InvestVoyager
providerReddit: Invest_Voyager

redirect_from:

---

On their website we read:

> **Advanced Security**<br>
  Offline storage, advanced fraud protection, and government-regulated processes
  keep your assets secure and your personal information safe.

which means this is a custodial offering and therefore **not verifiable**.
