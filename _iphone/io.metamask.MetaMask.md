---
wsId: metamask
title: "MetaMask - Blockchain Wallet"
altTitle: 
authors:
- leo
appId: io.metamask.MetaMask
appCountry: 
idd: 1438144202
released: 2020-09-03
updated: 2021-05-17
version: "2.3.0"
score: 3.44821
reviews: 531
size: 37363712
developerWebsite: https://metamask.io/
repository: 
issue: 
icon: io.metamask.MetaMask.jpg
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

This is an ETH-only app and thus not a Bitcoin wallet.
