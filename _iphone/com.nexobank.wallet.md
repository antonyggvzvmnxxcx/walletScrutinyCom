---
wsId: nexo
title: "Nexo - Crypto Banking Account"
altTitle: 
authors:
- leo
appId: com.nexobank.wallet
appCountry: 
idd: 1455341917
released: 2019-06-30
updated: 2021-05-03
version: "1.4.8"
score: 3.91228
reviews: 342
size: 34368512
developerWebsite: https://nexo.io
repository: 
issue: 
icon: com.nexobank.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

