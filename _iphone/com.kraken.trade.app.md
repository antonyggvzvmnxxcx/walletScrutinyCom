---
wsId: krakent
title: "Kraken Pro"
altTitle: 
authors:
- leo
appId: com.kraken.trade.app
appCountry: 
idd: 1473024338
released: 2019-11-12
updated: 2021-03-25
version: "1.5.13"
score: 4.65005
reviews: 7721
size: 33909760
developerWebsite: https://www.kraken.com
repository: 
issue: 
icon: com.kraken.trade.app.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-14
reviewStale: true
signer: 
reviewArchive:


providerTwitter: krakenfx
providerLinkedIn: krakenfx
providerFacebook: KrakenFX
providerReddit: 

redirect_from:

---

On their website we read:

> 95% of all deposits are kept in offline, air-gapped, geographically
  distributed cold storage. We keep full reserves so that you can always
  withdraw immediately on demand.

This app is an interface to a custodial exchange and therefore **not
verifiable**.
