---
wsId: ZelCore
title: "ZelCore"
altTitle: 
authors:
- leo
appId: com.zelcash.zelcore
appCountry: 
idd: 1436296839
released: 2018-09-23
updated: 2021-05-13
version: "v4.10.1"
score: 4.30909
reviews: 55
size: 55316480
developerWebsite: https://zel.network/zelcore
repository: 
issue: 
icon: com.zelcash.zelcore.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

