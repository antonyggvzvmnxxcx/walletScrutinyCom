---
wsId: Remitano
title: "Remitano"
altTitle: 
authors:
- leo
appId: com.remitano.remitano
appCountry: 
idd: 1116327021
released: 2016-05-28
updated: 2021-05-10
version: "5.31.0"
score: 4.76122
reviews: 6977
size: 52484096
developerWebsite: https://remitano.com
repository: 
issue: 
icon: com.remitano.remitano.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-23
reviewStale: true
signer: 
reviewArchive:


providerTwitter: remitano
providerLinkedIn: Remitano
providerFacebook: remitano
providerReddit: 

redirect_from:

---

This app is an interface to an exchange which holds your coins. On the App Store
and their website there is no claim to a non-custodial part to the app. As a
custodial app it is **not verifiable**.
