---
wsId: bitcointoyou
title: "Bitcointoyou Pro"
altTitle: 
authors:
- kiwilamb
appId: com.pro.b2u
appCountry: 
idd: 1489598378
released: 2019-12-20
updated: 2021-05-17
version: "1.73"
score: 5
reviews: 3
size: 117899264
developerWebsite: https://www.bitcointoyou.com
repository: 
issue: 
icon: com.pro.b2u.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-27
reviewStale: true
signer: 
reviewArchive:


providerTwitter: bitcointoyou
providerLinkedIn: bitcointoyou
providerFacebook: Bitcointoyou
providerReddit: 

redirect_from:

---

The [Bitcointoyou website](https://www.bitcointoyou.com) has no statement regarding the management of private keys.
However being an exchange, it is highly likely that this is a custodial service with funds being in control of the provider.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.