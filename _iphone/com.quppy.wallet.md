---
wsId: Quppy
title: "Quppy – Secure Bitcoin Walle‪t"
altTitle: 
authors:
- leo
appId: com.quppy.wallet
appCountry: 
idd: 1417802076
released: 2018-08-09
updated: 2021-04-16
version: "1.0.49"
score: 2.77778
reviews: 9
size: 46093312
developerWebsite: https://quppy.com
repository: 
issue: 
icon: com.quppy.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

