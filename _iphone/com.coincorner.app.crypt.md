---
wsId: coincorner
title: "CoinCorner - Bitcoin Wallet"
altTitle: 
authors:
- kiwilamb
appId: com.coincorner.app.crypt
appCountry: 
idd: 917721788
released: 2014-09-23
updated: 2021-04-12
version: "3.2.6"
score: 4.2
reviews: 10
size: 53079040
developerWebsite: https://www.coincorner.com
repository: 
issue: 
icon: com.coincorner.app.crypt.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-02
reviewStale: true
signer: 
reviewArchive:


providerTwitter: CoinCorner
providerLinkedIn: 
providerFacebook: CoinCorner
providerReddit: 

redirect_from:

---

A search of the app store and the providers website, reveals no statements about how private keys are managed.

This leads us to conclude the wallets funds are in control of the provider.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

