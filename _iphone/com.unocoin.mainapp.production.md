---
wsId: Unocoin
title: "Unocoin Wallet"
altTitle: 
authors:
- leo
appId: com.unocoin.mainapp.production
appCountry: 
idd: 1030422972
released: 2016-05-12
updated: 2021-04-16
version: "6.0.4"
score: 2.47826
reviews: 23
size: 208135168
developerWebsite: 
repository: 
issue: 
icon: com.unocoin.mainapp.production.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

