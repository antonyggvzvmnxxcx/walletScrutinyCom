---
wsId: eidoo
title: "Eidoo Ethereum Bitcoin Wallet"
altTitle: 
authors:
- leo
appId: io.eidoo.wallet.prodnet
appCountry: 
idd: 1279896253
released: 2017-09-23
updated: 2021-05-04
version: "2.18.0"
score: 3.79167
reviews: 72
size: 37786624
developerWebsite: https://eidoo.io
repository: 
issue: 
icon: io.eidoo.wallet.prodnet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

