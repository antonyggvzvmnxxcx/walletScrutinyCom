---
wsId: Luno
title: "Luno Bitcoin & Cryptocurrency"
altTitle: 
authors:
- leo
appId: za.co.Bitx
appCountry: 
idd: 927362479
released: 2014-11-03
updated: 2021-05-07
version: "7.13.0"
score: 4.4379
reviews: 3277
size: 88463360
developerWebsite: https://www.luno.com
repository: 
issue: 
icon: za.co.Bitx.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-12
reviewStale: true
signer: 
reviewArchive:


providerTwitter: LunoGlobal
providerLinkedIn: lunoglobal
providerFacebook: luno
providerReddit: 

redirect_from:

---

This app's Android version had a clear statement about being custodial in the
Play Store description but on the App Store, no claims are made that let you
infer the type of custody.

On the website under [security](https://www.luno.com/en/security) we found the
same claim as in the Android review though:

> **Deep freeze storage**<br>
  The majority of customer Bitcoin funds are kept in what we call “deep freeze”
  storage. These are multi-signature wallets, with private keys stored in
  different bank vaults.

which again is a clear statement of them holding your funds. As a custodial
service, this app is **not verifiable**.
