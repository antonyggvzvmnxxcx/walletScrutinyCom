---
wsId: klever
title: "Klever: Crypto Bitcoin Wallet"
altTitle: 
authors:
- leo
appId: cash.klever.blockchain.wallet
appCountry: 
idd: 1525584688
released: 2020-08-26
updated: 2021-05-12
version: "4.2.1"
score: 4.35417
reviews: 336
size: 124617728
developerWebsite: https://klever.io
repository: 
issue: 
icon: cash.klever.blockchain.wallet.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

