---
wsId: TokenPocket
title: "TokenPocket"
altTitle: 
authors:
- leo
appId: com.tp.ios
appCountry: 
idd: 1436028697
released: 2018-09-23
updated: 2021-05-15
version: "1.5.8"
score: 3.60504
reviews: 119
size: 126035968
developerWebsite: 
repository: https://github.com/TP-Lab/tp-ios
issue: https://github.com/TP-Lab/tp-ios/issues/1
icon: com.tp.ios.jpg
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-18
reviewStale: true
signer: 
reviewArchive:


providerTwitter: TokenPocket_TP
providerLinkedIn: 
providerFacebook: TokenPocket
providerReddit: 

redirect_from:

---

In contrast to the Android version, this app does not appear to support BTC.
