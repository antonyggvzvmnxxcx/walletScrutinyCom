---
wsId: ownbit
title: "Ownbit: Cold & MultiSig Wallet"
altTitle: 
authors:
- leo
appId: com.bitbill.wallet
appCountry: 
idd: 1321798216
released: 2018-02-07
updated: 2021-05-18
version: "4.30.1"
score: 4.42593
reviews: 54
size: 115529728
developerWebsite: 
repository: 
issue: 
icon: com.bitbill.wallet.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

