---
wsId: bitcoinblack
title: "Bitcoin Black Wallet"
altTitle: 
authors:
- kiwilamb
appId: com.bitcoinblack.wallet
appCountry: 
idd: 1523044877
released: 2020-07-20
updated: 2020-09-08
version: "1.0.5"
score: 4.22581
reviews: 31
size: 80958464
developerWebsite: https://bitcoin.black
repository: 
issue: 
icon: com.bitcoinblack.wallet.jpg
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-29
reviewStale: true
signer: 
reviewArchive:


providerTwitter: BCB_Official1
providerLinkedIn: 
providerFacebook: bitcoinblackofficial
providerReddit: AllAboardBitcoinBlack

redirect_from:

---

This is a wallet for an alt coin "Bitcoin Black"