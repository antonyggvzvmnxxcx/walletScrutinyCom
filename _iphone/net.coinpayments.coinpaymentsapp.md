---
wsId: CoinPayments
title: "CoinPayments - Crypto Wallet"
altTitle: 
authors:
- leo
appId: net.coinpayments.coinpaymentsapp
appCountry: 
idd: 1162855939
released: 2019-02-07
updated: 2021-03-17
version: "2.3.0"
score: 4.09091
reviews: 33
size: 133352448
developerWebsite: https://www.coinpayments.net/
repository: 
issue: 
icon: net.coinpayments.coinpaymentsapp.jpg
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

