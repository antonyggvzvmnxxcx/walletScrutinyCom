---
wsId: XCOEX
title: "XCOEX: Cryptocurrency Exchange"
altTitle: 
authors:
- kiwilamb
appId: com.xcoex.mobile
appCountry: 
idd: 1447945810
released: 2019-01-22
updated: 2021-05-06
version: "1.17.0"
score: 
reviews: 
size: 54894592
developerWebsite: https://xcoex.com/
repository: 
issue: 
icon: com.xcoex.mobile.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-04
reviewStale: true
signer: 
reviewArchive:


providerTwitter: OfficialXcoex
providerLinkedIn: 
providerFacebook: xcoex
providerReddit: 

redirect_from:

---

This is an exchange based app wallet, meaning it is mainly developed to manage trading on an exchange.
The exchange provider typically stores users bitcoins, partly in cold storage, partly hot.

This leads us to conclude the wallet funds are in control of the provider.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.
