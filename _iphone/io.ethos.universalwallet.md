---
wsId: ethosUW
title: "Ethos - Universal Wallet"
altTitle: 
authors:

appId: io.ethos.universalwallet
appCountry: 
idd: 1376959464
released: 2018-08-06
updated: 2019-11-22
version: "2.0.5"
score: 4.33779
reviews: 299
size: 72262656
developerWebsite: http://ethos.io
repository: 
issue: 
icon: io.ethos.universalwallet.jpg
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-05
reviewStale: true
signer: 
reviewArchive:


providerTwitter: ethos_io
providerLinkedIn: 
providerFacebook: ethosplatform
providerReddit: ethos_io

redirect_from:

---

They claim:

> STATE-OF-THE-ART SECURITY - No more managing multiple private keys and
  wallets. With the Universal Wallet, you generate a single Ethos SmartKey for
  each wallet instance you create, and it takes care of the rest – providing
  automated maximum security management of all of your digital assets. You
  remain in complete control at all times. Lose your phone? Simply restore your
  cryptocurrency wallet and regain control of your funds using your Ethos
  SmartKey. Self-Custody at its finest!

so they claim self-custody but using some [brand name] SmartKey which definitely
does not sound like a standard other wallets would support. What if they go out of
business? **Judging by the most recent ratings, that's exactly what happened
already** but the comments also sound like the SmartKey is just BIP39 24 words
mnemonic, so a broadly used standard after all.

We can't find any source code and thus consider the app **not verifiable**.
