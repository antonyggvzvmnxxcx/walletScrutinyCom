---
wsId: SpotWalletapp
title: "Buy Bitcoin - Spot Wallet app"
altTitle: 
authors:
- leo
appId: tech.spotapp.spot
appCountry: 
idd: 1390560448
released: 2018-08-07
updated: 2021-05-17
version: "3.3.2"
score: 4.61714
reviews: 3931
size: 85763072
developerWebsite: https://spot-bitcoin.com
repository: 
issue: 
icon: tech.spotapp.spot.jpg
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-14
reviewStale: true
signer: 
reviewArchive:


providerTwitter: spot_bitcoin
providerLinkedIn: spot-bitcoin
providerFacebook: spot.bitcoin
providerReddit: 

redirect_from:

---

On their website we read:

> **You control your Bitcoins.**<br>
  PayPal, Coinbase & Binance control your funds. We don't. You have entire
  control over your Bitcoins. We use the best technologies to ensure that your
  funds are always safe.

but as we cannot find any source code to check this claim, the wallet gets the
verdict **not verifiable**.
