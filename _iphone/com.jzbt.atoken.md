---
wsId: ATokenWallet
title: "AToken App"
altTitle: 
authors:
- kiwilamb
appId: com.jzbt.atoken
appCountry: 
idd: 1395835245
released: 2018-07-28
updated: 2021-04-29
version: "4.0.9"
score: 4.96498
reviews: 257
size: 178216960
developerWebsite: 
repository: 
issue: 
icon: com.jzbt.atoken.jpg
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: ATokenOfficial
providerLinkedIn: 
providerFacebook: ATokenOfficial
providerReddit: 

redirect_from:

---

Found on their support website...

> The AToken Wallet server does not save any private keys, mnemonics, and
  passwords for users, so mnemonics or lost private keys cannot be retrieved
  from the AToken wallet.<br>
  Please make sure that all users make backups and keep them properly. Do not
  share them with anyone.

The claim on their website is that the wallet is non-custodial, but without source code, this is **not verifiable**.
