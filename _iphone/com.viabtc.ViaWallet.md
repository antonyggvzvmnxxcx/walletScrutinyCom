---
wsId: ViaWallet
title: "ViaWallet - Multi-chain Wallet"
altTitle: 
authors:
- leo
appId: com.viabtc.ViaWallet
appCountry: 
idd: 1462031389
released: 2019-05-21
updated: 2021-05-13
version: "2.2.8"
score: 4.11111
reviews: 18
size: 78310400
developerWebsite: https://viawallet.com
repository: 
issue: 
icon: com.viabtc.ViaWallet.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

