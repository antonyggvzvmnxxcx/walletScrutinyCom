---
wsId: Sylo
title: "Sylo"
altTitle: 
authors:
- leo
appId: io.sylo.dapp
appCountry: 
idd: 1452964749
released: 2019-09-10
updated: 2021-05-04
version: "3.0.22"
score: 4.78571
reviews: 42
size: 179801088
developerWebsite: https://www.sylo.io/wallet/
repository: 
issue: 
icon: io.sylo.dapp.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

