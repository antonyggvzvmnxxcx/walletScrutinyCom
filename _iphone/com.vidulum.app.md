---
wsId: Vidulum
title: "Vidulum"
altTitle: 
authors:
- leo
appId: com.vidulum.app
appCountry: 
idd: 1505859171
released: 2020-07-28
updated: 2021-05-12
version: "1.2.1"
score: 4.33333
reviews: 6
size: 62133248
developerWebsite: https://vidulum.app
repository: 
issue: 
icon: com.vidulum.app.jpg
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

