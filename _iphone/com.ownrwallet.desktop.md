---
wsId: OWNR
title: "OWNR crypto wallet for PC"
altTitle: 
authors:
- leo
appId: com.ownrwallet.desktop
appCountry: 
idd: 1520395378
released: 2020-08-13
updated: 2021-02-25
version: "1.4.1"
score: 
reviews: 
size: 123824490
developerWebsite: https://ownrwallet.com
repository: 
issue: 
icon: com.ownrwallet.desktop.png
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-22
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

