---
wsId: 
title: "Algorand Wallet"
altTitle: 
authors:

users: 50000
appId: com.algorand.android
launchDate: 
latestUpdate: 2021-05-11
apkVersionName: "4.8.1"
stars: 4.9
ratings: 2439
reviews: 759
size: 37M
website: 
repository: 
issue: 
icon: com.algorand.android.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-06
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.algorand.android/
---


