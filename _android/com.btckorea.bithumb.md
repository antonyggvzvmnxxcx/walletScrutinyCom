---
wsId: bithumbko
title: "Bithumb - No.1 Digital Asset Platform"
altTitle: 
authors:
- leo
users: 1000000
appId: com.btckorea.bithumb
launchDate: 
latestUpdate: 2021-05-07
apkVersionName: "2.1.9"
stars: 3.5
ratings: 17441
reviews: 7811
size: 36M
website: https://www.bithumb.com
repository: 
issue: 
icon: com.btckorea.bithumb.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-19
reviewStale: true
signer: 
reviewArchive:


providerTwitter: BithumbOfficial
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app is an interface to an exchange and to our knowledge only features
custodial accounts and therefore is **not verifiable**.
