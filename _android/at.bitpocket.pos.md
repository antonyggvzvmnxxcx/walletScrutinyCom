---
wsId: 
title: "BitPocket"
altTitle: 
authors:
- kiwilamb
users: 100
appId: at.bitpocket.pos
launchDate: 
latestUpdate: 2017-10-20
apkVersionName: "1.1.8"
stars: 4.8
ratings: 5
reviews: 3
size: 4.7M
website: http://bitpocket.at/
repository: https://github.com/getbitpocket/bitpocket-mobile-app
issue: 
icon: at.bitpocket.pos.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-15
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


A Bitcoin wallet.
