---
wsId: 
title: "Volt:Bitcoin,ETH&BSV Crypto Wallet,Multisig Wallet"
altTitle: 
authors:

users: 100
appId: bitmesh.volt.wallet
launchDate: 
latestUpdate: 2021-05-12
apkVersionName: "1.2.2"
stars: 4.4
ratings: 32
reviews: 20
size: 22M
website: 
repository: 
issue: 
icon: bitmesh.volt.wallet.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-13
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


