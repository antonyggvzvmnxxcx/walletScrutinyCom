---
wsId: 
title: "BINANCE CRYPTO WALLET APP"
altTitle: 
authors:

users: 5000
appId: binance.cryptowalletapp
launchDate: 
latestUpdate: 2021-04-05
apkVersionName: "9.8"
stars: 3.9
ratings: 14
reviews: 11
size: 9.4M
website: 
repository: 
issue: 
icon: binance.cryptowalletapp.png
bugbounty: 
verdict: defunct # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-05
reviewStale: true
signer: 
reviewArchive:
- date: 2021-04-13
  version: 
  apkHash: 
  gitRevision: 7ef50d1754a724355ce59937abb663d96fd262d2
  verdict: wip


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


Unfortunately we did not get to review this one before it was removed from
Google Play.