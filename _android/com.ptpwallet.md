---
wsId: PTPWallet
title: "PTPWallet - Bitcoin, Ethereum, and Other Crypto"
altTitle: 
authors:
- leo
users: 10000
appId: com.ptpwallet
launchDate: 
latestUpdate: 2021-05-10
apkVersionName: "1.0.1221"
stars: 4.0
ratings: 329
reviews: 242
size: 24M
website: https://ptpwallet.com
repository: 
issue: 
icon: com.ptpwallet.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-06
reviewStale: true
signer: 
reviewArchive:


providerTwitter: PtpWallet
providerLinkedIn: 
providerFacebook: PTPWalletPage
providerReddit: perkscoin

redirect_from:
  - /com.ptpwallet/
---


There are no very explicit claims made about where the private keys are stored
but reading between the lines:

> PTPWallet has low transaction fees when withdrawing crypto coins like Bitcoin,
  Ethereum, Litecoin, Ripple, Ethereum Cash, Ethereum Classic, and other digital
  assets.

"withdrawing crypto" only makes sense when you don't control the coins yet.

On the website there is no claims to the contrary and so we assume this is a
custodial offering and thus **not verifiable**.
