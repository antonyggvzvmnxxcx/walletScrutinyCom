---
wsId: YouHodler
title: "YouHodler - Crypto and Bitcoin Wallet"
altTitle: 
authors:
- leo
users: 100000
appId: com.youhodler.youhodler
launchDate: 
latestUpdate: 2021-05-12
apkVersionName: "2.14.0"
stars: 4.4
ratings: 1328
reviews: 525
size: 47M
website: https://youhodler.com
repository: 
issue: 
icon: com.youhodler.youhodler.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-06-20
reviewStale: true
signer: 
reviewArchive:


providerTwitter: youhodler
providerLinkedIn: youhodler
providerFacebook: YouHodler
providerReddit: 

redirect_from:
  - /com.youhodler.youhodler/
  - /posts/com.youhodler.youhodler/
---


This app is the interface to an exchange and might have a non-custodial part to
it but if so, it is not well advertised on their website and we assume it is
custodial and therefore **not verifiable**.
