---
wsId: coincloud
title: "Coin Cloud Wallet"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: com.bitpay.coincloud
launchDate: 
latestUpdate: 2021-04-27
apkVersionName: "11.2.21"
stars: 3.6
ratings: 128
reviews: 58
size: 18M
website: https://www.coincloudatm.com/
repository: 
issue: 
icon: com.bitpay.coincloud.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-12
reviewStale: true
signer: 
reviewArchive:


providerTwitter: CoinCloudATM
providerLinkedIn: 
providerFacebook: coincloudATM
providerReddit: 

redirect_from:

---


It is very clear that the provider is claiming that this wallet is non-custodial with this early statement found in the [play store description](https://play.google.com/store/apps/details?id=com.bitpay.coincloud).

> Keep your bitcoin and other digital currency secure and under your own control with the non-custodial Coin Cloud Wallet app.

With keys in control of the user, we need to find the source code in order to check reproducibility. However we are unable to locate a public source repository.

Our verdict: As there is no source code to be found anywhere, this wallet is at best a non-custodial closed source wallet and as such **not verifiable**.

