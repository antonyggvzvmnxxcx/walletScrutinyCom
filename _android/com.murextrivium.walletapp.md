---
wsId: 
title: "Murex Trivium (Bitcoin Lightning wallet)"
altTitle: 
authors:
- kiwilamb
users: 50
appId: com.murextrivium.walletapp
launchDate: 
latestUpdate: 2018-09-23
apkVersionName: "1.2"
stars: 0.0
ratings: 
reviews: 
size: 6.0M
website: http://murexbitcoinsolutions.com/
repository: 
issue: 
icon: com.murextrivium.walletapp.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-15
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


A Bitcoin Lightning wallet.
