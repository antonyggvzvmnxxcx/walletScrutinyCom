---
wsId: 
title: "Citowise - Blockchain multi-currency wallet"
altTitle: 
authors:
- kiwilamb
users: 50000
appId: com.citowise.wallet
launchDate: 
latestUpdate: 2019-12-25
apkVersionName: "1.355"
stars: 4.2
ratings: 655
reviews: 524
size: 11M
website: https://www.citowise.com
repository: 
issue: 
icon: com.citowise.wallet.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-30
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


There is a clear statement on the [Play store](https://play.google.com/store/apps/details?id=com.citowise.wallet) about private key management.

> Citowise is one of the few wallets that is both safe and decentralized as it does not keep private user keys. 

With keys in control of the user, we need to find the source code in order to check reproducibility.
However we are unable to locate a public source repository.

Unfortunately we were not able to load the main [providers website](https://www.citowise.com), receiving a 404 response.
After several attempts we can only concluded this wallet may not be supported anymore with a defunct provider.

Our verdict: As there is no source code to be found anywhere, this wallet is at best a non-custodial closed source wallet and as such **not verifiable**.

