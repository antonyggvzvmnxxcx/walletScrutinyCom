---
wsId: SpotWalletapp
title: "Buy Bitcoin, cryptocurrency - Spot BTC wallet"
altTitle: 
authors:
- leo
users: 50000
appId: com.spot.spot
launchDate: 
latestUpdate: 2021-05-13
apkVersionName: "4.28.1.2600-c793c0d3"
stars: 4.3
ratings: 3961
reviews: 1931
size: 65M
website: https://www.spot-bitcoin.com
repository: 
issue: 
icon: com.spot.spot.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-11-16
reviewStale: true
signer: 
reviewArchive:


providerTwitter: spot_bitcoin
providerLinkedIn: spot-bitcoin
providerFacebook: spot.bitcoin
providerReddit: 

redirect_from:
  - /com.spot.spot/
---


On their website we read:

> **You control your Bitcoins.**
> 
> PayPal, Coinbase & Binance control your funds. We don't. You have entire
  control over your Bitcoins. We use the best technologies to ensure that your
  funds are always safe.

but as we cannot find any source code to check this claim, the wallet gets the
verdict **not verifiable**.
