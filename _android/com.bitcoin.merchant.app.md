---
wsId: 
title: "Bitcoin Cash Register (BCH)"
altTitle: 
authors:

users: 100000
appId: com.bitcoin.merchant.app
launchDate: 2019-04-13
latestUpdate: 2021-05-06
apkVersionName: "5.3.7"
stars: 4.0
ratings: 390
reviews: 147
size: 6.0M
website: https://www.bitcoin.com/bitcoin-cash-register
repository: 
issue: 
icon: com.bitcoin.merchant.app.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-12-25
reviewStale: true
signer: 
reviewArchive:


providerTwitter: bitcoincom
providerLinkedIn: 
providerFacebook: buy.bitcoin.news
providerReddit: btc

redirect_from:
  - /com.bitcoin.merchant.app/
  - /posts/com.bitcoin.merchant.app/
---


This is a watch-only wallet according to their description:

> Just enter either a standard Bitcoin Cash address or an “extended public key”
(aka an “xpub”) from your Bitcoin Cash wallet to start accepting instant and
secure Bitcoin Cash payments at your business.

As it doesn't manage private keys, you cannot spend with it and consequently
neither the provider can steal or lose your funds.
