---
wsId: 
title: "Mobile LN"
altTitle: 
authors:
- kiwilamb
users: 10
appId: com.mobileln
launchDate: 
latestUpdate: 2019-04-11
apkVersionName: "pre-alpha v0.2b"
stars: 0.0
ratings: 
reviews: 
size: 38M
website: 
repository: https://github.com/hihidev/MobileLN
issue: 
icon: com.mobileln.png
bugbounty: 
verdict: defunct # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-30
reviewStale: true
signer: 
reviewArchive:
- date: 2021-04-15
  version: 
  apkHash: 
  gitRevision: 7c41675d933938883582fc5a083d69e8b2644900
  verdict: fewusers


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


**Update 2021-04-30**: This app is no more available on Google Play.

### Original Analysis

A Bitcoin + Lightning wallet.
