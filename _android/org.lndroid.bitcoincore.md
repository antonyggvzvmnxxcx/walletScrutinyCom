---
wsId: 
title: "BitcoinCore for Android"
altTitle: 
authors:

users: 1000
appId: org.lndroid.bitcoincore
launchDate: 
latestUpdate: 2020-07-08
apkVersionName: "0.6"
stars: 4.0
ratings: 5
reviews: 1
size: 7.5M
website: 
repository: 
issue: 
icon: org.lndroid.bitcoincore.png
bugbounty: 
verdict: wip # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-08
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


<!-- https://gitlab.com/walletscrutiny/walletScrutinyCom/-/issues/189 -->
