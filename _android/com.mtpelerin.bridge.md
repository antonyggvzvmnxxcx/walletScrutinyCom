---
wsId: 
title: "Bridge Wallet, the Swiss app for Bitcoin & Crypto"
altTitle: 
authors:
- leo
users: 5000
appId: com.mtpelerin.bridge
launchDate: 
latestUpdate: 2021-05-06
apkVersionName: "1.14"
stars: 4.5
ratings: 52
reviews: 30
size: 77M
website: https://www.mtpelerin.com/bridge-wallet
repository: 
issue: 
icon: com.mtpelerin.bridge.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-27
reviewStale: true
signer: 
reviewArchive:


providerTwitter: mtpelerin
providerLinkedIn: mt-pelerin
providerFacebook: mtpelerin
providerReddit: MtPelerin

redirect_from:
  - /com.mtpelerin.bridge/
---


On Google Play they claim

> **YOU ARE IN CONTROL**<br>
  Bridge Wallet is a decentralized, non-custodial wallet. It means that you are
  in full control of your wallet, its content and seed phrase.

But while the provider [has a GitHub presence](https://github.com/MtPelerin),
there is no claim about public source and neither do we find any wallet
repository on their GitHub.

As a closed source wallet, this is **not verifiable**.
