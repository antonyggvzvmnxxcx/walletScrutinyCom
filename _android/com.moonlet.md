---
wsId: 
title: "Moonlet"
altTitle: 
authors:

users: 10000
appId: com.moonlet
launchDate: 
latestUpdate: 2021-05-18
apkVersionName: "1.4.39"
stars: 4.1
ratings: 264
reviews: 173
size: 9.2M
website: 
repository: 
issue: 
icon: com.moonlet.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-12-14
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.moonlet/
---


This app appears to only support ETH tokens. Neither the description, nor the
website claim otherwise.
