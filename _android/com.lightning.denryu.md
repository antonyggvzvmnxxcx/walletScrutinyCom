---
wsId: 
title: "Denryu Wallet"
altTitle: 
authors:
- kiwilamb
users: 500
appId: com.lightning.denryu
launchDate: 
latestUpdate: 2018-12-26
apkVersionName: "0.1"
stars: 2.5
ratings: 17
reviews: 12
size: 6.2M
website: https://denryu.hashhub.tokyo/
repository: 
issue: 
icon: com.lightning.denryu.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-15
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This is a lightning wallet.

