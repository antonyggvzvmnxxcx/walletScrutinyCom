---
wsId: 
title: "monerujo: Monero Wallet"
altTitle: 
authors:
- leo
users: 50000
appId: com.m2049r.xmrwallet
launchDate: 
latestUpdate: 2021-05-05
apkVersionName: "2.0.6 'Puginarug'"
stars: 3.5
ratings: 692
reviews: 416
size: Varies with device
website: https://monerujo.io
repository: 
issue: 
icon: com.m2049r.xmrwallet.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-02-27
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This app does not feature BTC wallet functionality.