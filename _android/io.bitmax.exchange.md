---
wsId: 
title: "AscendEX(BitMax)"
altTitle: 
authors:
- kiwilamb
- leo
users: 100000
appId: io.bitmax.exchange
launchDate: 
latestUpdate: 2021-04-25
apkVersionName: "2.4.7"
stars: 4.2
ratings: 2788
reviews: 947
size: 22M
website: https://ascendex.com
repository: 
issue: 
icon: io.bitmax.exchange.png
bugbounty: 
verdict: defunct # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-11
reviewStale: true
signer: 
reviewArchive:
- date: 2021-04-20
  version: "2.4.7"
  apkHash: 
  gitRevision: 6849790cf3f18653fbe1116b54693fec1419d0ca
  verdict: custodial


providerTwitter: AscendEX_Global
providerLinkedIn: 
providerFacebook: AscendEXOfficial
providerReddit: AscendEX_Official

redirect_from:

---


This app apparently was removed in favor of [AscendEx](/android/com.ascendex.exchange/).