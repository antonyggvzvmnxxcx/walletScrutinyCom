---
wsId: 
title: "Paymint - Secure Bitcoin Wallet"
altTitle: 
authors:
- leo
users: 100
appId: com.paymintlabs.paymint
launchDate: 2020-06-29
latestUpdate: 2020-10-05
apkVersionName: "1.2.2"
stars: 4.3
ratings: 9
reviews: 5
size: 25M
website: 
repository: https://github.com/Paymint-Labs/Paymint
issue: 
icon: com.paymintlabs.paymint.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2020-10-03
reviewStale: true
signer: 
reviewArchive:


providerTwitter: paymint_wallet
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /com.paymintlabs.paymint/
---


