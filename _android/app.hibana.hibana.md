---
wsId: 
title: "Hibana Wallet"
altTitle: 
authors:
- kiwilamb
users: 50
appId: app.hibana.hibana
launchDate: 
latestUpdate: 2018-12-11
apkVersionName: "0.1"
stars: 0.0
ratings: 
reviews: 
size: 6.5M
website: 
repository: 
issue: 
icon: app.hibana.hibana.png
bugbounty: 
verdict: fewusers # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-15
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This is a lightning wallet.
