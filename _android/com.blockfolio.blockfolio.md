---
wsId: 
title: "Blockfolio: Buy Bitcoin Now"
altTitle: 
authors:

users: 1000000
appId: com.blockfolio.blockfolio
launchDate: 2015-10-01
latestUpdate: 2021-05-08
apkVersionName: "3.0.31"
stars: 4.5
ratings: 142714
reviews: 43013
size: 59M
website: https://www.blockfolio.com
repository: 
issue: 
icon: com.blockfolio.blockfolio.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-11-10
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
  - /blockfolio/
  - /com.blockfolio.blockfolio/
  - /posts/2019/11/blockfolio/
  - /posts/com.blockfolio.blockfolio/
---


This is not a wallet.
