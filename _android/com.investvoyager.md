---
wsId: voyager
title: "Voyager - Buy Bitcoin & Crypto"
altTitle: 
authors:
- leo
users: 500000
appId: com.investvoyager
launchDate: 
latestUpdate: 2021-05-06
apkVersionName: "2.5.19"
stars: 3.6
ratings: 4624
reviews: 2872
size: 44M
website: https://www.investvoyager.com/
repository: 
issue: 
icon: com.investvoyager.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-01-02
reviewStale: true
signer: 
reviewArchive:


providerTwitter: investvoyager
providerLinkedIn: investvoyager
providerFacebook: InvestVoyager
providerReddit: Invest_Voyager

redirect_from:
  - /com.investvoyager/
---


On their website we read:

> **Advanced Security**<br>
  Offline storage, advanced fraud protection, and government-regulated processes
  keep your assets secure and your personal information safe.

which means this is a custodial offering and therefore **not verifiable**.
