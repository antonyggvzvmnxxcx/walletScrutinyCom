---
wsId: XCOEX
title: "XCOEX: Cryptocurrency Exchange & Blockchain Wallet"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: com.xcoex.mobile
launchDate: 
latestUpdate: 2021-05-06
apkVersionName: "1.17.0"
stars: 3.7
ratings: 150
reviews: 120
size: 37M
website: https://xcoex.com/
repository: 
issue: 
icon: com.xcoex.mobile.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-04
reviewStale: true
signer: 
reviewArchive:


providerTwitter: OfficialXcoex
providerLinkedIn: 
providerFacebook: xcoex
providerReddit: 

redirect_from:

---


This is an exchange based app wallet, meaning it is mainly developed to manage trading on an exchange.
The exchange provider typically stores users bitcoins, partly in cold storage, partly hot.

This leads us to conclude the wallet funds are in control of the provider.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.
