---
wsId: 
title: "Simple Bitcoin Widget"
altTitle: 
authors:

users: 100000
appId: com.brentpanther.bitcoinwidget
launchDate: 
latestUpdate: 2021-05-17
apkVersionName: "7.4.2"
stars: 4.0
ratings: 1813
reviews: 710
size: 3.5M
website: 
repository: 
issue: 
icon: com.brentpanther.bitcoinwidget.png
bugbounty: 
verdict: nowallet # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-03-05
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


> Note: This is only a widget. You must add the widget to your launcher, it will
  not appear in your apps list.
