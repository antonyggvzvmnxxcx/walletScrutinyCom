---
wsId: 
title: "Crypto Pay"
altTitle: 
authors:
- kiwilamb
users: 1000
appId: com.cryptopay
launchDate: 
latestUpdate: 2019-02-14
apkVersionName: "1.5"
stars: 0.0
ratings: 
reviews: 
size: 9.1M
website: https://shamlatech.com/
repository: 
issue: 
icon: com.cryptopay.png
bugbounty: 
verdict: nosource # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-15
reviewStale: true
signer: 
reviewArchive:


providerTwitter: shamlatech
providerLinkedIn: shamlatech
providerFacebook: shamlatechsolutions
providerReddit: 

redirect_from:

---


This wallet claims to be non-custodial but we cannot find any source code on their [official Website page](https://shamlatech.com/).

Our verdict: This app is **not verifiable**.

Tried to make contact to discover source code repository...
- Website chat - no response
- Telegram - cannot find the posted user/group.
- Whatsapp - wanted to make contact via a phone number which i declined. 
