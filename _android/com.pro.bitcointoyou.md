---
wsId: bitcointoyou
title: "Bitcointoyou Pro"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: com.pro.bitcointoyou
launchDate: 
latestUpdate: 2021-05-15
apkVersionName: "0.0.170"
stars: 3.8
ratings: 968
reviews: 617
size: 50M
website: https://www.bitcointoyou.com
repository: 
issue: 
icon: com.pro.bitcointoyou.jpg
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-27
reviewStale: true
signer: 
reviewArchive:


providerTwitter: bitcointoyou
providerLinkedIn: bitcointoyou
providerFacebook: Bitcointoyou
providerReddit: 

redirect_from:

---


The [Bitcointoyou website](https://www.bitcointoyou.com) has no statement regarding the management of private keys.
However being an exchange, it is highly likely that this is a custodial service with funds being in control of the provider.

Our verdict: This 'wallet' is custodial and therefore **not verifiable**.


