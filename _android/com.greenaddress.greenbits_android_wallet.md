---
wsId: GreenBitcoinWallet
title: "Green: Bitcoin Wallet"
altTitle: 
authors:
- leo
users: 100000
appId: com.greenaddress.greenbits_android_wallet
launchDate: 2015-01-01
latestUpdate: 2021-05-03
apkVersionName: "3.5.9"
stars: 3.8
ratings: 779
reviews: 432
size: 75M
website: https://blockstream.com/green
repository: https://github.com/Blockstream/green_android/
issue: https://github.com/Blockstream/green_android/issues/116
icon: com.greenaddress.greenbits_android_wallet.png
bugbounty: 
verdict: nonverifiable # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-15
reviewStale: false
signer: 32f9cc00b13fbeace51e2fb51df482044e42ad34a9bd912f179fedb16a42970e
reviewArchive:
- date: 2021-05-03
  version: "3.5.9"
  apkHash: f62c0b4b4882ad15a561fbabcccc34739d66470e752a21a4eab4037736593476
  gitRevision: 13d8e0095e0944d8d255811487d819fafc74c5e1
  verdict: nonverifiable
- date: 2021-05-03
  version: "3.5.8"
  apkHash: 950446b62e9e1a536a2341155949750b9856a24768374aac3ce74f2e91394356
  gitRevision: a393a14039f9ee960578b19999c30df46191dd01
  verdict: reproducible
- date: 2021-04-01
  version: "3.5.4"
  apkHash: 4ed9729881676b84d7ed65b0f0bd583c11c465186e896e96888c5d323e8c5002
  gitRevision: 512303fa6c495727005df2cb8e1c853128ee03ca
  verdict: reproducible
- date: 2021-02-05
  version: "3.4.9"
  apkHash: fd146d68348e332a6a6e2f548c473599ba684cbb6107328a3871de09259f00e5
  gitRevision: 58e8db9ca9f5b5ec1a1881ad5a74fb402bcf3438
  verdict: reproducible
- date: 2021-01-18
  version: "3.4.8"
  apkHash: ef539fe60af20a538eb4bdaad37a8cde12cb873cca97800749a71a5add0e9ff7
  gitRevision: 8eb6570934dad87e4b32ae8c8d7e6f06ed8aae43
  verdict: reproducible
- date: 2021-01-03
  version: "3.4.5"
  apkHash: efa5e3e56b1081bb66ca99d3fea7d5b1375a8f30696acf0938a324ba12c5458c
  gitRevision: 6f2db0822de5b16cad4f1a7459068a27a50c4896
  verdict: reproducible
- date: 2020-12-15
  version: "3.4.4"
  apkHash: d54f84856c25c302978ed5c23ad01c3c0c89930f8f9cd2098558563d9f8b1a3e
  gitRevision: 9817359e09ab4cc05136dc7dfbb40d950750ec4f
  verdict: reproducible
- date: 2020-11-08
  version: "3.4.2"
  apkHash: e631aef67a2d50cced4f3a2a850e6f32950e0a91e12678678441defa3da71681
  gitRevision: e855e82e36403f60cfebfd66a8126f9c7dc1cfd4
  verdict: reproducible
- date: 2020-10-17
  version: "3.4.1"
  apkHash: 991b1d5672faed19ee8e96a66f7b6812e23971eaf28187424c9af41c4ff16d82
  gitRevision: 84ced1caca6883b917853741705a4b70e7c40ef9
  verdict: reproducible
- date: 2020-10-07
  version: "3.4.0"
  apkHash: fb7d9611ad878ef4116b525a50255f3b16725ec673a5c717f14c5d021b242188
  gitRevision: 84ced1caca6883b917853741705a4b70e7c40ef9
  verdict: reproducible
- date: 2020-08-31
  version: "3.3.9"
  apkHash: 6a4a4bb05c0c087c4b85486f01982a8bf1bde91a70c587e22929e9faed3eb6ed
  gitRevision: ea0cf3403d57a0e33f7d7627d9854b737fc0d62e
  verdict: nonverifiable
- date: 2020-07-11
  version: "3.3.8"
  apkHash: 3a0a02ea8ccd791ab3ec24bac4d45249164f5c53366538b5befcbd4df3f6edb3
  gitRevision: 09ea9943f4ad41f83d28027e3275105483849996
  verdict: reproducible
- date: 2020-05-06
  version: "3.3.7"
  apkHash: 847a67a5cfa498cf2e137b0a4306202322a35d4e3fba6bb90a269709e26e11ab
  gitRevision: 4392a6481b289b2cb82d790db15fa8feadf40b1d
  verdict: reproducible
- date: 2020-04-26
  version: "3.3.6"
  apkHash: b91e3c6e35aa9223c7d1f62498b162fde226db38049016a354f87578fde371ab
  gitRevision: 9cc435e13b5b513f7ecdaa1baee739b2683d2ba5
  verdict: reproducible
- date: 2020-03-14
  version: "3.3.5"
  apkHash: e30092950197aa2801b0f958a90496cf182f76e790f2d7e82e08dbe01b7c32c8
  gitRevision: a6b2771dbc314160ba304573fd0a6cc5d6d1ccb9
  verdict: reproducible
- date: 2020-02-17
  version: "3.3.4"
  apkHash: f88686a2e41718b82ba8d2f5ff7eb8d0ada044d29711e75f0128104bbee40baf
  gitRevision: bb658ba9291af3d814ba8a00c4726c9584e379d1
  verdict: reproducible
- date: 2020-01-18
  version: "3.3.2"
  apkHash: 7e48e2ff0e8d484f4000b7d96bbdc6b0939a76d8ca80355b5ebedbf68511f77c
  gitRevision: d47e69dd99ac700665328b92b3026a2cf6e36960
  verdict: reproducible
- date: 2020-01-09
  version: "3.3.0"
  apkHash: b2e3f2d437bba5d97f3b331aac20616d3312e34d25023c38c42483974828cdec
  gitRevision: 0d558ec280ca9606901f6557622af98e0cbdc97b
  verdict: reproducible
- date: 2019-11-23
  version: "3.2.7"
  apkHash: 8b2e67fc333eeef5b10ce6f9f5fc3e4ca104c1eca9c539b73805276e09d838db
  gitRevision: 3d972d9773b0fd2fb1602d31117a50be01d48610
  verdict: reproducible

providerTwitter: Blockstream
providerLinkedIn: blockstream
providerFacebook: Blockstream
providerReddit: 

redirect_from:
  - /greenwallet/
  - /com.greenaddress.greenbits_android_wallet/
  - /posts/2019/11/greenwallet/
  - /posts/com.greenaddress.greenbits_android_wallet/
---


With
[this script](https://gitlab.com/walletscrutiny/walletScrutinyCom/blob/master/test.sh)
we get:

```
Results:
appId:          com.greenaddress.greenbits_android_wallet
signer:         32f9cc00b13fbeace51e2fb51df482044e42ad34a9bd912f179fedb16a42970e
apkVersionName: 3.5.9
apkVersionCode: 22000359
apkHash:        76cc3df154ff6d47b5366a328515cf14c1d550ca7d71063d851c1949324ef4fa

Diff:
Files /tmp/fromPlay_com.greenaddress.greenbits_android_wallet_22000359/apktool.yml and /tmp/fromBuild_com.greenaddress.greenbits_android_wallet_22000359/apktool.yml differ
Only in /tmp/fromPlay_com.greenaddress.greenbits_android_wallet_22000359/original/META-INF: GREENADD.RSA
Only in /tmp/fromPlay_com.greenaddress.greenbits_android_wallet_22000359/original/META-INF: GREENADD.SF
Only in /tmp/fromPlay_com.greenaddress.greenbits_android_wallet_22000359/original/META-INF: MANIFEST.MF
Files /tmp/fromPlay_com.greenaddress.greenbits_android_wallet_22000359/res/drawable-xxhdpi/ic_tor_60.png and /tmp/fromBuild_com.greenaddress.greenbits_android_wallet_22000359/res/drawable-xxhdpi/ic_tor_60.png differ

Revision, tag (and its signature):
object 215fd772c93093e56f43c42f3b87651901639623
type commit
tag release_3.5.9
tagger Domenico Gabriele <domenico@blockstream.com> 1619702421 +0200

Release 3.5.9
```

which is unfortunately a tiny deviation from the file we got from the Play
Store.

The result is almost the same as what we got when testing 3.5.9 from GitHub
releases. As there is a difference of one png while those files are identical
according to diffoscope:

```
$ diffoscope 'Green 3.5.9 (com.greenaddress.greenbits_android_wallet).apk' green-producition-release-3.5.9.apk
--- Green 3.5.9 (com.greenaddress.greenbits_android_wallet).apk
+++ green-producition-release-3.5.9.apk
│┄ Format-specific differences are supported for Android APK files but no file-specific differences were detected; falling back to a binary diff. file(1) reports: Zip archive data, at least v?[0] to extract
@@ -4194297,8 +4194297,8 @@
 03ffff80: 1721 0091 0400 0014 e203 40f9 1f03 00f1  .!........@.....
 03ffff90: 4d11 0054 4800 40f9 1f01 1beb 6100 0054  M..TH.@.....a..T
 03ffffa0: 488c 5ff8 1b05 4091 6903 08cb 29fd 4493  H._...@.i...).D.
 03ffffb0: 6a13 18cb 3f01 18eb 5ac1 889a 09c3 899a  j...?...Z.......
 03ffffc0: 7f03 1aeb e227 00a9 200b 0054 f603 1baa  .....'.. ..T....
 03ffffd0: 1100 0014 e91f 8052 2901 08cb ca3f 8052  .......R)....?.R
 03ffffe0: 4801 08cb 3f01 00f1 08b1 899a 0afd 4893  H...?.........H.
-[ Too much input for diff (SHA256: 41d752c80082e8870af194f33c2d1f47045f7fc291cb226092f3dd4c29358e2d) ]
+[ Too much input for diff (SHA256: 733f97db16bdaadfc3dd0008e8dd0e442eec889bb65c46349c89cfb6320b2d25) ]
```

We assume something about the png creation is non-deterministic but almost
deterministic.

Either way although it all looks very benign, we have to give the verdict
**not verifiable**.
