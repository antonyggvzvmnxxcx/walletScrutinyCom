---
wsId: metamask
title: "MetaMask - Buy, Send and Swap Crypto"
altTitle: 
authors:
- leo
users: 1000000
appId: io.metamask
launchDate: 
latestUpdate: 2021-05-12
apkVersionName: "2.2.0"
stars: 2.9
ratings: 4989
reviews: 2818
size: 28M
website: https://metamask.io
repository: 
issue: 
icon: io.metamask.png
bugbounty: 
verdict: nobtc # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-01
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---


This is an ETH-only app and thus not a Bitcoin wallet.
