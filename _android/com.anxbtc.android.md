---
wsId: 
title: "ANX Vault: Your Bitcoin Wallet"
altTitle: 
authors:
- kiwilamb
users: 10000
appId: com.anxbtc.android
launchDate: 
latestUpdate: 2016-08-17
apkVersionName: "1.8.0"
stars: 3.2
ratings: 136
reviews: 58
size: 4.1M
website: https://anxpro.com/
repository: 
issue: 
icon: com.anxbtc.android.png
bugbounty: 
verdict: defunct # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-05-08
reviewStale: true
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:

---

On the main site there is a [announcement that the provider is closing](https://anxpro.com/) and all funds should be withdrawn from its services.<br>
It would be prudent to believe the wallet is no longer supported.

