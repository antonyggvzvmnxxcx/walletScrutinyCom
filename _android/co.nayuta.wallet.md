---
title: "Nayuta - Bitcoin Lightning Wallet"
altTitle: 

users: 500
appId: co.nayuta.wallet
launchDate: 
latestUpdate: 2019-10-15
apkVersionName: "Varies with device"
stars: 
ratings: 
reviews: 
size: Varies with device
website: https://nayuta.co
repository: 
issue: 
icon: co.nayuta.wallet.png
bugbounty: 
verdict: defunct # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2019-12-29
reviewStale: false
signer: 
reviewArchive:


providerTwitter: 
providerLinkedIn: 
providerFacebook: 
providerReddit: 

redirect_from:
---


This app disappeared from Google Play before we got to have a look at it. With
500 downloads it was not due yet.
