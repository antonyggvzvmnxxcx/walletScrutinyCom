---
wsId: pointpay
title: "PointPay Bank: Cryptocurrency Wallet & Exchange"
altTitle: 
authors:
- kiwilamb
users: 50000
appId: com.pointpay.bank
launchDate: 
latestUpdate: 2021-05-17
apkVersionName: "5.3.5"
stars: 4.8
ratings: 2093
reviews: 1157
size: 71M
website: https://wallet.pointpay.io
repository: 
issue: 
icon: com.pointpay.bank.png
bugbounty: 
verdict: custodial # wip fewusers nowallet nobtc obfuscated custodial nosource nonverifiable reproducible bounty defunct
date: 2021-04-26
reviewStale: true
signer: 
reviewArchive:


providerTwitter: PointPay1
providerLinkedIn: pointpay
providerFacebook: PointPayLtd
providerReddit: PointPay

redirect_from:

---


The PointPay website has very little information about how they manage private keys of the user.
The only basic statement is...

> We use strong military-grade encryption to store private keys

we will have to conclude the wallet funds are in control of the provider and hence custodial.

Our verdict: This 'wallet' is custodial and therefore is **not verifiable**.

